package cl.ubb.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.*;


import cl.ubb.dao.LibrosDao;
import cl.ubb.model.Libro;

public class LibrosService {
	
	private LibrosDao libroDao;
	
	public Libro crearLibro(Libro libro) {
		// TODO Auto-generated method stub
		return libroDao.save(libro);
		
	}

	public Libro obtenerLibro(long id) {
		// TODO Auto-generated method stub
		Libro libro = new Libro();
		
		//libro.setId(id);
		libro = libroDao.findOne(id);
		return libro;
		
	}
	
	public ArrayList<Libro> obtenerTodos() {
		
		ArrayList<Libro> misLibros = (ArrayList<Libro>) libroDao.findAll();		
		return misLibros;
	}
	
	public Libro obtenerLibro(String titulo) {
		// TODO Auto-generated method stub
		Libro libro = new Libro();
		
		libro = libroDao.findByTitulo(titulo);
		return libro;
		
	}


}
