package cl.ubb;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.runners.MockitoJUnitRunner;


import cl.ubb.dao.LibrosDao;
import cl.ubb.model.Libro;
import cl.ubb.services.LibrosService;

@RunWith(MockitoJUnitRunner.class)
public class LibrosServicesTest {
	
	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService;
	
//	private Libro libro;
	
/*	@Before
	public void SetUp(){
		libro = new Libro();
		libro.setId(1L);
	}
*/	
	
	@Test
	public void deboCrearUnLibroYRetornarlo(){
		//arrange
		Libro libro = new Libro();
		
		
		//act
		when(librosDao.save(libro)).thenReturn(libro);
		Libro libroCreado = librosService.crearLibro(libro);
		
		//assert
		Assert.assertNotNull(libroCreado);
		Assert.assertEquals(libro, libroCreado);
	}

	@Test
	public void obtenerTodosLosLibros(){
		//arrange
		List<Libro> misLibros = new ArrayList<Libro>();
		//act
		when(librosDao.findAll()).thenReturn(misLibros);
		ArrayList<Libro> resultado = librosService.obtenerTodos();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misLibros, resultado);
		  
	}
	

	
}
